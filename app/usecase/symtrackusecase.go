package usecase

import (
	"symptomstracker/app"
)

// UsecaseCharidyAddress ...
type UsecaseCharidyAddress struct {
	rCharidyAddress app.Repository
}

// NewUsecase ...
func NewUsecase(rCharidyAddress app.Repository) app.Usecase {

	return &UsecaseCharidyAddress{
		rCharidyAddress: rCharidyAddress,
	}

}

// GetCountryByAddressID ... ctx context.Context,
func (u *UsecaseCharidyAddress) GetCountryByAddressID(addressID uint) error {
	err := u.rCharidyAddress.GetList()
	if err != nil {
		return nil
	}

	// item, err := u.rCharidyAddress.GetCountryByID(ctx, address.CountryID)
	// if err != nil {
	// 	return nil, errors.WithStack(err)
	// }
	return nil
}
