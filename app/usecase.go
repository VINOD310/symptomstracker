package app

// Usecase ...
type Usecase interface {
	GetCountryByAddressID(addressID uint) error
}
