package delivery

import (
	"errors"
	"net/http"
	"symptomstracker/app"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type Config struct {
	DB     *gorm.DB
	Router *echo.Echo
}

// Handler  represent the httphandler
type Handler struct {
	uCharidyAddress app.Usecase
}

// NewHTTPHandler ...
func NewHTTPHandler(
	r *echo.Echo,
	uCharidyAddress app.Usecase,
) *echo.Echo {

	handler := &Handler{
		uCharidyAddress: uCharidyAddress,
	}

	r.POST("/login", handler.login)
	r.GET("/users", handler.testUser)

	return r

}

func (handler Handler) testUser(c echo.Context) error {

	result := map[string]interface{}{
		"message": "ngpbackend",
	}

	if err := handler.uCharidyAddress.GetCountryByAddressID(1); err != nil {
		c.JSON(http.StatusOK, errors.New("error happen"))
	}

	return c.JSON(http.StatusOK, result)
}
