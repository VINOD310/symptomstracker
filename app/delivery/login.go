package delivery

import (
	"encoding/json"
	"net/http"
	"strconv"
	"symptomstracker/jwtauth"
	"symptomstracker/models"

	"github.com/labstack/echo"
)

func (handler Handler) login(c echo.Context) error {

	var user models.User
	if err := json.NewDecoder(c.Request().Body).Decode(&user); err != nil {
		return err
	}

	token, err := jwtauth.CreateToken(strconv.Itoa(user.ID))
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	return c.JSON(http.StatusOK, token)
}
