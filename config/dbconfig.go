package config

import (
	"fmt"

	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/postgres"

	_ "github.com/lib/pq"
)

func ConfigDB() *gorm.DB {
	DBConnection := fmt.Sprintf("host=%s port=%v user=%s dbname=%s password=%s sslmode=%s",
		"127.0.0.1", 5432, "postgres", "symptomstracker", "root", "disable")
	DB, err := gorm.Open("postgres", DBConnection)

	if err != nil {
		panic(err)
	}
	return DB
}
